'''
   Copyright (C) 2019 neo.Foletto

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''

'''
Created on Nov 11, 2019

@author: neo
'''
import matplotlib.pyplot as plt
import numpy

# Plot gráfico de barras
class PlotGraphicBar(object):

  def __init__(self, Y, rounded, accuracy):
    labels = ['Anormal', 'Normal']
    countZero = 0
    for i in range(Y.size):
      if (Y[i] == 0):
        countZero = countZero + 1
    datasetCount = [countZero, Y.size - countZero]

    countZero = 0
    for i in range(len(rounded)):
      if (rounded[i] == 0):
        countZero = countZero + 1
    predictionsCount = [countZero, Y.size - countZero]

    x = numpy.arange(len(labels))  # the label locations
    width = 0.3  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width / 2, datasetCount, width, label='Observado')
    rects2 = ax.bar(x + width / 2, predictionsCount, width, label='Previsto - acurácia: %.2f%%' % (accuracy * 100))

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Tamanho do conjunto')
    ax.set_title('Conjunto de dados de sintomas de dor lombar')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()

    def autolabel(rects):
      """Attach a text label above each bar in *rects*, displaying its height."""
      for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)

    fig.tight_layout()

    plt.show()
